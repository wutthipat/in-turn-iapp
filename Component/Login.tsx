import { Card, Col, Row } from 'antd';
import React from 'react';

const App: React.FC = () => (
  <div className="site-card-wrapper">
    <Row gutter={16} justify="end"  style={{paddingTop:'100px',paddingRight:'180px'}}>
      <Col span={8}>
        <Card
        hoverable  
        bordered={true}
        >
            <img src="/iconNavbar.png"  style={{ width:'220px', height:'220px', display:'blok', margin:'auto' }}></img>
          <p style={{paddingTop:'50px', textAlign: 'center'}}>65555</p>
        </Card>
      </Col>
    </Row>
  </div>
);

export default App;