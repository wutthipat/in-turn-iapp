import { Breadcrumb, Layout, Menu } from 'antd';
import React from 'react';

const { Header, Content, Footer } = Layout;

const App: React.FC = () => (
  <Layout>
    <Header style={{ background:'#064595', position: 'fixed', zIndex: 1, width: '100%' }}>
      <div className="logo" 
      />
      <div><img className="logo" src="/iconNavbar.png"></img></div>
      <Menu
        style={{background:'#064595', borderColor: '#064595',color: '#ffffff'}}
        mode="horizontal"
        defaultSelectedKeys={['0']}
        items={new Array(1).fill(null).map((_, index) => ({
          key: String(index + 1),
          label: `โครงการระบบบริการสารสนเทศของกระทรวงมหาดไทยด้วยโครงข่ายเสมือน`,
        }))}
      />
    </Header>
    
  </Layout>
);

export default App;